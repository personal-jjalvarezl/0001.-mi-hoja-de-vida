# Mi hoja de vida en LaTeX

Ultima versión de mi CV, con fuente Helvetica utilizando colores inspirados en 
Monokai (hay una opción `print` el cual renderiza en blanco y negro y revierte 
la cabecera a negro sobre blanco en caso de que sea necesario imprimir sobre 
papel).

Utiliza TikZ para la cabecera, XeTeX/LuaLaTeX y fontspec para usar Helvetica 
Neue, biber para imprimir mis publicaciones y textpos al lado.

## Standalone build

Before running this: The current `.tex` file contains placeholders as `<<<my_placeholder>>>` that will not be replaced in the PDF file, so is better to execute the entire project.

* Install the `make` command through `build-essential` package.
* Install the `biber` compiler through `biber` package.
* Install `ghostscript` package as a pdf compressor.
* Install latex (LuaLaTeX + XeTeX) through `texlive-full` package.
* Install the SourceSansPro font to compile the PDF with this font.
* The `config.yml` file contains the rest of the commands you need to run to execute the pdf file.
