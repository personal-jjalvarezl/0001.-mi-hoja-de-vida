INPUT_FILE="./cv.tex"
ANNEXES_FILE="./annexes.tex"
DESTINATION_PATH="$(pwd)/../../.build"
SHRINKPDF_PATH="$(pwd)/../../utilities"
TRANSLATIONS_PATH="$(pwd)/../../i18n"
SRC_SYMLINKS=(
    "./resources"
    "./bibliography.bib"
    "./friggeri-cv.cls"
)
WHICH_BASH=`which bash`
WHICH_LUALATEX=`which lualatex`
LUALATEX_EXEC="$WHICH_LUALATEX -interaction=nonstopmode"
WHICH_BIBER=`which biber`
SHRINKPDF_BASH_FILE="$SHRINKPDF_PATH/shrinkpdf.sh"
DPI_COMPRESSION_QUALITY=120 # Default: 72

echo "*******************************"
echo "Variables:"
echo " - INPUT_FILE: $INPUT_FILE"
echo " - ANNEXES_FILE: $ANNEXES_FILE"
echo " - DESTINATION_PATH: $DESTINATION_PATH"
echo " - SHRINKPDF_PATH: $SHRINKPDF_PATH"
echo " - TRANSLATIONS_PATH: $TRANSLATIONS_PATH"
echo " - SRC_SYMLINKS: $SRC_SYMLINKS"
echo " - WHICH_BASH: $WHICH_BASH"
echo " - LUALATEX_EXEC: $LUALATEX_EXEC"
echo " - WHICH_BIBER: $WHICH_BIBER"
echo " - SHRINKPDF_BASH_FILE: $SHRINKPDF_BASH_FILE"
echo " - DPI_COMPRESSION_QUALITY: $DPI_COMPRESSION_QUALITY"
echo "*******************************"


mkdir -p $DESTINATION_PATH

for symlink in "${SRC_SYMLINKS[@]}"; do
    ln -sf "$PWD/$symlink" $DESTINATION_PATH
done

COMPILATION_FILES=(`$WHICH_BASH $TRANSLATIONS_PATH/i18n.sh --input-file $INPUT_FILE \
    --annexes-file $ANNEXES_FILE --destination-path $DESTINATION_PATH \
    --translations-path $TRANSLATIONS_PATH`)

for compilation_file in "${COMPILATION_FILES[@]}"; do
    echo
    echo "*******************************"
    echo " $compilation_file COMPILATION "
    echo "*******************************"
    echo
    cd $DESTINATION_PATH
    $LUALATEX_EXEC $compilation_file > ${compilation_file%.*}-lualatex-build.log
    $WHICH_BIBER ${compilation_file%.*} > ${compilation_file%.*}-biber-build.log
    $LUALATEX_EXEC $compilation_file > ${compilation_file%.*}-lualatex-build.log
    echo
    echo "*******************************"
    echo " $compilation_file COMPRESSION "
    echo "*******************************"
    echo
    cd $SHRINKPDF_PATH
    echo "Shrink code: $WHICH_BASH $SHRINKPDF_BASH_FILE $DESTINATION_PATH/${compilation_file%.*}.pdf $DESTINATION_PATH/${compilation_file%.*}-compressed.pdf $DPI_COMPRESSION_QUALITY"
    $WHICH_BASH $SHRINKPDF_BASH_FILE $DESTINATION_PATH/${compilation_file%.*}.pdf $DESTINATION_PATH/${compilation_file%.*}-compressed.pdf $DPI_COMPRESSION_QUALITY
done

echo "Build finished"