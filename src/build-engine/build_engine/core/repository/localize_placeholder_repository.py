import os
from typing import Callable, List
from ..model import LocalizeModel


class LocalizePlaceholderRepository():
    def __init__(self, entity_factory: Callable[..., LocalizeModel]) -> None:
        self.entity_factory = entity_factory

    def replace_all(self, placeholders: List[LocalizeModel], src: str, destination: str) -> None:
        content: str = ""
        with open(src, mode="r", encoding="utf-8") as file:
            content = file.read()
        for placeholder in placeholders:
            if placeholder.value:
                content = content.replace(
                    f"<<<{placeholder.key}>>>", str(placeholder.value))
        with open(destination, mode="w", encoding="utf-8") as file:
            file.write(content)
        os.chmod(destination, 0o777)
