import json
from typing import Callable, List
from ..model import LocalizeModel


class LocalizeJsonRepository():
    def __init__(self, entity_factory: Callable[..., LocalizeModel]) -> None:
        self.entity_factory = entity_factory

    def get_all(self, path: str) -> List[LocalizeModel]:
        data = json.load(open(file=path, mode="r", encoding="utf-8"))
        all_traductions: List[LocalizeModel] = []
        for key in data.keys():
            all_traductions.append(LocalizeModel(key=key, value=data[key]))
        return all_traductions
