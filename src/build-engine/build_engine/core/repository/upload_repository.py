import google.oauth2.service_account
import googleapiclient.discovery  # type: ignore
from googleapiclient.http import MediaFileUpload  # type: ignore
import os
import socket
from typing import List


class UploadRepository():
    """Upload Repository"""

    def __init__(self) -> None:
        socket.setdefaulttimeout(None)

        credentials = google.oauth2.service_account.Credentials.from_service_account_info(info={
            "type": os.getenv("GOOGLE_OAUTH2_TYPE"),
            "project_id": os.getenv("GOOGLE_OAUTH2_PROJECT_ID"),
            "private_key_id": os.getenv("GOOGLE_OAUTH2_PRIVATE_KEY_ID"),
            "private_key": os.getenv("GOOGLE_OAUTH2_PRIVATE_KEY", "").replace('\\n', '\n'),
            "client_email": os.getenv("GOOGLE_OAUTH2_CLIENT_EMAIL"),
            "client_id": os.getenv("GOOGLE_OAUTH2_CLIENT_ID"),
            "auth_uri": os.getenv("GOOGLE_OAUTH2_AUTH_URI"),
            "token_uri": os.getenv("GOOGLE_OAUTH2_TOKEN_URI"),
            "auth_provider_x509_cert_url": os.getenv("GOOGLE_OAUTH2_AUTH_PROVIDER_X509_CERT_URL"),
            "client_x509_cert_url": os.getenv("GOOGLE_OAUTH2_CLIENT_X509_CERT_URL"),
            "universe_domain": os.getenv("GOOGLE_OAUTH2_UNIVERSE_DOMAIN")
        })

        self.drive_service = googleapiclient.discovery.build(
            'drive', 'v3', credentials=credentials)

        raw_dir_ids: str = os.getenv("GOOGLE_DRIVE_PARENT_DIRS", "")

        self.dir_ids: list[str] = raw_dir_ids.split(
            ",") if raw_dir_ids.split(",") else [raw_dir_ids]

    def _delete_files(self, file_or_folder_id):
        """Delete a file or folder in Google Drive by ID."""
        try:
            self.drive_service.files().delete(fileId=file_or_folder_id).execute()
            print(
                f"\tSuccessfully deleted file/folder with ID: {file_or_folder_id}")
        except Exception as e:
            print(f"\tError deleting file/folder with ID: {file_or_folder_id}")
            print(f"\tError details: {str(e)}")

    def clear_dirs(self):
        """Clear upliaded files and folders in Google Drive."""
        items = []
        for dir_id in self.dir_ids:
            results = self.drive_service.files().list(
                q=f"'{dir_id}' in parents and trashed=false",
                pageSize=1000,
                fields="nextPageToken, files(id, name, mimeType)"
            ).execute()
            items.extend(results.get('files', []))

        if not items:
            print("\tNo folders or files found in Google Drive.")
        else:
            print("\tFolders and files in Google Drive:")
            for item in items:
                print(f"\tRemoving: {item}")
                self._delete_files(item['id'])

    def upload(self, file_path: str):
        """Upload the file to google drive to my-cv directory"""
        file_metadata = {
            'name': file_path.split("/")[-1],
            'parents': self.dir_ids
        }
        media = MediaFileUpload(file_path)
        file = self.drive_service.files().create(
            body=file_metadata, media_body=media).execute()
        print(f"\tFile {file} uploaded")
