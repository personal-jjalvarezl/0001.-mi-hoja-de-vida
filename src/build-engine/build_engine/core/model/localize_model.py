class LocalizeModel:
    def __init__(self, **kwfields) -> None:
        self.key = kwfields.get("key")
        self.value = kwfields.get("value")