from typing import List, Optional
from shutil import rmtree
import os
import sys
import subprocess
import yaml
from ..repository import LocalizeJsonRepository, LocalizePlaceholderRepository


class BuildService():
    def __init__(self, localize_json_repository: LocalizeJsonRepository, localize_placeholder_repository: LocalizePlaceholderRepository) -> None:
        self.localize_json_repository = localize_json_repository
        self.localize_placeholder_repository = localize_placeholder_repository
        current_dir_path = os.path.dirname(
            sys.modules['__main__'].__file__ or "")
        self.assets_path = f"{current_dir_path}/assets"
        self.build_path = f"{current_dir_path}/../../../.build"
        self.shrink_pdf_utility_path = f"{current_dir_path}/../../../utilities/shrinkpdf.sh "

    def _clean_build_dir(self) -> None:
        if os.path.exists(self.build_path):
            rmtree(self.build_path)
        if not os.path.exists(self.build_path):
            os.mkdir(self.build_path, 0o777)
            os.mkdir(f"{self.build_path}/release", 0o777)

    def _set_env_variables(self, asset: str, localizer: str) -> None:
        """ Set the environment variables for config.yml files used for all assets """
        os.environ["DOC_LOCALIZER"] = localizer
        os.environ["DOC_NAME"] = asset
        os.environ["RELEASE_DIR_PATH"] = f"{self.build_path}/release/{asset}"
        os.environ["SHRINK_PDF_PATH"] = self.shrink_pdf_utility_path

    def _symlink_assets_subdirs(self, asset: Optional[str]) -> List[str]:
        """ Creates correct symlinking for all necessary files to make cv building
            It does not symlink the .tex files since they are going to be changed by localization. """
        if asset is not None:
            assets = [asset]
        else:
            assets = [o for o in os.listdir(self.assets_path) if os.path.isdir(
                os.path.join(self.assets_path, f"{o}/latex/"))]
        for asset_index in assets:
            os.mkdir(f"{self.build_path}/{asset_index}", 0o777)
            for o in [o for o in os.listdir(f"{self.assets_path}/{asset_index}/latex/") if not o.endswith(".tex")]:
                os.symlink(f"{self.assets_path}/{asset_index}/latex/{o}",
                           f"{self.build_path}/{asset_index}/{o}")
        return assets

    def _localize_tex_files(self, asset: str, localization_file: str) -> None:
        """ Execute localization by each localization file it can found """
        localizers = self.localize_json_repository.get_all(
            f"{self.assets_path}/{asset}/localization/{localization_file}")
        for o in [o for o in os.listdir(f"{self.assets_path}/{asset}/latex/") if o.endswith(".tex")]:
            self.localize_placeholder_repository.replace_all(
                localizers, f"{self.assets_path}/{asset}/latex/{o}", f"{self.build_path}/{asset}/{o}")

    def _build_latex(self, asset: str, localizer: str) -> None:
        self._set_env_variables(asset, localizer)
        if not os.path.exists(f"{self.build_path}/release/{asset}"):
            os.mkdir(f"{self.build_path}/release/{asset}", 0o777)
        build_commands: List[str] = []
        with open(f"{self.build_path}/{asset}/config.yml", encoding="utf-8") as file:
            build_commands = yaml.load(
                file, Loader=yaml.FullLoader).get("buildCommands")
        os.chdir(f"{self.build_path}/{asset}/")
        for build_command in build_commands:
            print(f"\t\tExecuting: {build_command} for {asset}")
            p = subprocess.Popen(build_command, shell=True, stdout=open(
                f"{self.build_path}/release/{asset}/build_engine-{localizer}.out", "w", encoding="utf-8"))
            p.wait()

    def _build_document(self, asset: str) -> None:
        """ Individual build of a document """
        for o in [o for o in os.listdir(f"{self.assets_path}/{asset}/localization/")]:
            localizer = o.split(".")[0]
            print(f"\tLocalizing {asset} for {localizer}")
            self._localize_tex_files(asset, o)
            print(f"\tBuilding {asset} for {localizer}")
            self._build_latex(asset, localizer)

    def build(self, asset: Optional[str]) -> None:
        """ Final function to be called to produce every latex file possible. """
        print("Documents build started")
        self._clean_build_dir()
        assets = self._symlink_assets_subdirs(asset)
        for asset_index in assets:
            self._build_document(asset_index)
            
        print("Documents build finished")
