from typing import List, Optional
import os
import sys
from ..repository import UploadRepository


class UploadService():
    """ Service to upload latex's pdf documents to google drive. """

    def __init__(self, upload_repository: UploadRepository) -> None:
        self.upload_repository: UploadRepository = upload_repository
        current_dir_path = os.path.dirname(
            sys.modules['__main__'].__file__ or "")
        self.release_path = f"{current_dir_path}/../../../.build/release"

    def _pdfs_by_asset(self, asset: str) -> List[str]:
        """ Get all the pdf file paths for a single assets. """
        pdfs: List[str] = []
        for o in [o for o in os.listdir(f"{self.release_path}/{asset}") if o.endswith(".pdf")]:
            pdfs.append(f"{self.release_path}/{asset}/{o}")
        return pdfs

    def _retrieve_pdfs(self, asset: Optional[str]) -> List[str]:
        """ Get all the pdf file paths for all the assets. """
        pdfs: List[str] = []
        if asset is not None:
            pdfs = self._pdfs_by_asset(asset)
        else:
            for asset_index in [o for o in os.listdir(self.release_path)]:
                pdfs.extend(self._pdfs_by_asset(asset_index))
        return pdfs

    def upload(self, asset: Optional[str]):
        """ Final function to be called to upload all produced pdf files. """
        print("Removing Google Drive uploaded pdfs")
        self.upload_repository.clear_dirs()
        print("Google Drive pdfs removed")
        print("Documents upload started")
        pdfs = self._retrieve_pdfs(asset)
        for pdf in pdfs:
            print(f"\tUploading {pdf} file")
            self.upload_repository.upload(pdf)
            print(f"\tFile {pdf} uploaded")
        print("Documents upload finished")
