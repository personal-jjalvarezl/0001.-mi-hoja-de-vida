import argparse
import os
import sys
from dependency_injector.wiring import Provide, inject
from . import ApplicationContainer, BuildService, UploadService


@inject
def main(
    build_service: BuildService = Provide[
        ApplicationContainer.build_service_container.build_service
    ],
    upload_service: UploadService = Provide[
        ApplicationContainer.upload_service_container.upload_service
    ]
) -> None:
    assets_path = f'{os.path.dirname(sys.modules["__main__"].__file__ or "")}/assets'
    assets = [o for o in os.listdir(assets_path)]

    parser = argparse.ArgumentParser(description="LaTeX build engine")
    parser.add_argument("-t", "--type", help="Execution type",
                        choices=["all", "build", "upload"], default="build", required=True)
    parser.add_argument("-a", "--asset", help="Asset to be processed",
                        choices=assets, required=False)

    args = parser.parse_args()

    if args.type == "all":
        build_service.build(args.asset)
        upload_service.upload(args.asset)
    elif args.type == "build":
        build_service.build(args.asset)
    elif args.type == "upload":
        upload_service.upload(args.asset)
    else:
        raise Exception(f"Execution type {args.type} not supported")


if __name__ == '__main__':
    application = ApplicationContainer()
    application.wire(modules=[__name__])
    main()
