from build_engine.core import UploadService, UploadRepository
from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Provider, Dependency, Singleton


class UploadServiceContainer(DeclarativeContainer):
    """ Upload service container. """

    upload_repository: Provider[UploadRepository] = Dependency(
        instance_of=UploadRepository)

    upload_service: Provider[UploadService] = Singleton(
        UploadService,
        upload_repository
    )
