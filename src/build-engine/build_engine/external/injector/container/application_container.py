from dependency_injector.providers import Container
from dependency_injector.containers import DeclarativeContainer
from .localize_repository_container import LocalizeRepositoryContainer
from .upload_repository_container import UploadRepositoryContainer
from .build_service_container import BuildServiceContainer
from .upload_service_container import UploadServiceContainer


class ApplicationContainer (DeclarativeContainer):
    """ Application Container. """
    localize_repository_container = Container(
        LocalizeRepositoryContainer
    )

    upload_repository_container = Container(
        UploadRepositoryContainer
    )

    build_service_container = Container(
        BuildServiceContainer,
        localize_json_repository=localize_repository_container.localize_json_repository,
        localize_placeholder_repository=localize_repository_container.localize_placeholder_repository
    )

    upload_service_container = Container(
        UploadServiceContainer,
        upload_repository=upload_repository_container.upload_repository,
    )
