from build_engine.core import LocalizeModel, LocalizeJsonRepository, LocalizePlaceholderRepository
from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Factory, Singleton


class LocalizeRepositoryContainer(DeclarativeContainer):
    """ Localize Repository Container. """

    localize_factory = Factory(LocalizeModel)

    localize_json_repository = Singleton(
        LocalizeJsonRepository,
        entity_factory=localize_factory.provider
    )

    localize_placeholder_repository = Singleton(
        LocalizePlaceholderRepository,
        entity_factory=localize_factory.provider
    )
