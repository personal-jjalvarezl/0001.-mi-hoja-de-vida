from build_engine.core import (
    BuildService, LocalizeJsonRepository, LocalizePlaceholderRepository)
from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Provider, Dependency, Singleton


class BuildServiceContainer(DeclarativeContainer):
    """ Service to build latex documents. """

    localize_json_repository: Provider[LocalizeJsonRepository] = Dependency(
        instance_of=LocalizeJsonRepository)

    localize_placeholder_repository: Provider[LocalizePlaceholderRepository] = Dependency(
        instance_of=LocalizePlaceholderRepository)

    build_service: Provider[BuildService] = Singleton(
        BuildService,
        localize_json_repository,
        localize_placeholder_repository
    )
