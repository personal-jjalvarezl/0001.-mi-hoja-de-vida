from build_engine.core import UploadRepository
from dependency_injector.containers import DeclarativeContainer
from dependency_injector.providers import Singleton


class UploadRepositoryContainer(DeclarativeContainer):
    """ Upload Repository Container """

    upload_repository = Singleton(UploadRepository)
