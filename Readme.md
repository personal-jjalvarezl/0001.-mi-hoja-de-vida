# Build engine

[![CircleCI](https://dl.circleci.com/status-badge/img/bb/personal-jjalvarezl/0001.-mi-hoja-de-vida/tree/build-engine.svg?style=svg)](https://dl.circleci.com/status-badge/redirect/bb/personal-jjalvarezl/0001.-mi-hoja-de-vida/tree/build-engine)

The build engine executes every latex document automatically, also is oriented to support internationalization of LaTeX documents.

## Preconditions

Some environment variables containing sensitive information, have to be set in order to execute correctly the docker image in which this project is running.

Create a `.devcontainer/.env` file to support these new variables, most of them are supported in Google Accounts APIs. The only exception is the `GOOGLE_DRIVE_PARENT_DIRS` env variable that contains a list of strings, and represent the directory IDs in which the PDF files are going to be uploaded.

```properties
GOOGLE_OAUTH2_TYPE="<incoming_google_api_value>"
GOOGLE_OAUTH2_PROJECT_ID="<incoming_google_api_value>"
GOOGLE_OAUTH2_PRIVATE_KEY_ID="<incoming_google_api_value>"
GOOGLE_OAUTH2_PRIVATE_KEY="<incoming_google_api_value>"
GOOGLE_OAUTH2_CLIENT_EMAIL="<incoming_google_api_value>"
GOOGLE_OAUTH2_CLIENT_ID="<incoming_google_api_value>"
GOOGLE_OAUTH2_AUTH_URI="<incoming_google_api_value>"
GOOGLE_OAUTH2_TOKEN_URI="<incoming_google_api_value>"
GOOGLE_OAUTH2_AUTH_PROVIDER_X509_CERT_URL="<incoming_google_api_value>"
GOOGLE_OAUTH2_CLIENT_X509_CERT_URL="<incoming_google_api_value>"
GOOGLE_OAUTH2_UNIVERSE_DOMAIN="<incoming_google_api_value>"
GOOGLE_DRIVE_PARENT_DIRS="<parend_dir_id_1>,<parend_dir_id_2>,...,<parend_dir_id_n>"
```

For more information about where to look at the `<incoming_google_api_value>`'s. These values are the private key for google cloud projects. Look at [this](https://dev.to/binaryibex/python-and-google-drive-how-to-list-and-create-files-and-folders-2023-2nmm) for more information.

## Installation

### 1st Method: Visual Studio Code's Dev containers extension + Docker compose (recommended)

This method is recommended as far as you can get all the features for VSCode installed and running, ready for development, with linting and highlights necesary to improve the code. This is a combination of the following two methods:

1. Execute the entire 2nd Method: This will create the image (and he container as well) to build the documents. Notice that, the created container stack is `build-engine` -> `build-1`. You can delete these container stacks at any point after finishing the 2nd method process to save host resources.
2. Execute the entire 3rd Method: This will create all the features we need as indicated in the `.devcontainer/devcontainer.json` file, including the container stack and the vscode extensions. The configuration of these vscode extensions already exists inside the `.vscode/settings.json` file.
3. Follow the login configuration for circle-ci extension.

### 2nd Method: Docker compose (minimal installation, just documents generation)

1. Install [docker-desktop](https://www.docker.com/products/docker-desktop/) for your O.S.
2. Go to `.devcontainer/` dir.
3. Depending of the O.S. there are some ready to execute commands:
    * For windows. The `.devcontainer/powershell.ps1` is the powershell file with powershell commands:
        * Execute the `.devcontainer/powershell.ps1` file in powershell: `. powershell.ps1`.
        * After this, all the functions are loaded into powershell, just type the function name in the powershell and execute it.
    * For linux / unix. Ensure the `make` command is working on your O.S.:
        * In the shell, you can execute commands as this way:`make <Makefile_command>`
4. Based on the 3rd point, execute the `run-services` function / command to build the container with all the dependencies.

### 3rd Method: Visual Studio Code's Dev containers extension (not recomended by separate)

Visual Studio Code's Dev containers extension (`ms-vscode-remote.remote-containers`) can create the docker container:

1. Open this project in VSCode.
2. Install Dev containers extension (`ms-vscode-remote.remote-containers`). Or just ther recommended extensions in the `.vscode/extensions.json` file.
3. Make click on the low left corner of the IDE, at the "Open a Remote Window" icon (Similar to `><`).
4. A contextual menu should be opened. Then make click into the "Reopen in Container" option.

This will create a vscode session inside the container.

This method is not recommended because the container creation takes too much time and hangs vscode.

This method is recommended but after the container is already created. This part will be expanded later on this installation section.

### 4th Method: Host (minimal installation, just documents generation)

Apply the commands inside `.devcontainer/builder-files/builder.dockerfile` to your particular O.S.

Of course, you can modify the vscode documents to match your host configurations, just avoid commiting them.

## Commands description

The `./docker/Makefile` contains all the necesary commands to execute all:

| Linux - Unix | Windows (powershell) | Description |
|--------------|----------------------|-------------|
| `run-services` | `run-services`     | Builds everything for local execution inside the `builder` container, including O.S. libraries as well as pyenv + virtualenv + python environment with respective pip requirements.txt dependencies installed.|
| `build-docs-outside` | `build-docs` | Builds the docs once the above command is executed successfully. This means, the container was successfully created |
| `builder-start-session` | `builder-start-session` | starts a bash session inside container. Just in case you need to make some modifications. |
| `clean` | `clean` | Deletes all the contents of the `.build` directory. |
| `build-docs` | N/A | This command is exclusively to be executed inside a container session. Build the documents inside the container |

## How the build engine works

1. Lets say than an asset is a LaTeX compilable project in terms of this repository.
2. Every asset is located inside `.src/build-engine/build_engine/assets` directory following the next structure:
    * `.src/build-engine/build_engine/assets/<latex-project>/latex`: the directory storing the latex project.
    * `.src/build-engine/build_engine/assets/<latex-project>/latex/config.yml`: this file tells the build engine how to produce the final pdf files by using shell commands.
    * `.src/build-engine/build_engine/assets/<latex-project>/localization`: a directory which contains `.json` files with placeholder strings to replace on `.tex` files on above directory.
3. The build engine starts by organizing the each latex project insde the assets directory by using symlinks inside the `./build` directory except the .tex files.
4. Next step is copy the `.tex` files of a i-th latex project, replaces each string placeholder inside the same file, identified with a `<<<my_placeholder>>>`, with the ones of the localization folder and then send them into the build directory,
5. The engine reads the `config.yml` file to get all the steps in sequence to produce the final file by using shell commands.
6. The final results will be stored inside `./.build/release` by following the next format:
    * `./.build/relese/<latex-project>/build-engine-<json_file_name>.out`: which contains all the stdout for latex production.
    * `./.build/relese/<latex-project>/<main_tex_file_name>-<json_file_name>.<produced_file_ext>.`: the final produced file
    * Other outputs that you want to introduce, for example, by using the `./utilities/shrinkpdf.sh` file.

## Extra considerations

### The .tex files string placeholders

* For each `.tex` file you introduce, the placeholder string must be following the format `<<<placeholder_string>>>`
* The `placeholder_string` then should be present for each `.json` file you want to create.

### The `config.yml` environment variables list

Some environment variables can be stored inside the `config.yml` to being executed, the engine will create the following:

* `DOC_LOCALIZER`: the localizer (json file name inside the `localization`` dir) that is being used in real time by each compilation regarding to the json file the is being processed.
* `DOC_NAME`: the name of the document (A.K.A. the name of the directory inside the `assets` one).
* `RELEASE_DIR_PATH`: the release path by each file produced
* `SHRINK_PDF_PATH`: the shrink pdf utility to compress the final produced pdf.

## Example of a build

* This is by using docker, this will be the same locally
* Some considerations of this particular `docker compose` command:
  * The `'-l'` flag simulates a login session to source the `/etc/profile` file wich calls `/etc/profile.d/pyenv.sh` one, that is in charge of loading pyenv + virtualenv commands.
  * The `-c` flag represents the real commands to be ran through the session initialized with `/etc/profile` calls
  * The rest of the `./devcontainer/Makefile` have this similar principle.
* Regarding the `./devcontainer/Makefile`:
  * Rule `build-docs-outside` builds the assets
  * Rule `upload-docs-outside` uploads the build assets into Google Cloud
  * Rule `all-processes-outside` builds and uploads the assets.
  * The above rules support a parameter called `ASSET` to process a single asset instead all the existing assets.

``` text
$ cd .devcontainer
$ make build-docs-outside ASSET=complete-cv
docker compose -p "build-engine" exec -it builder /bin/bash '-l' -c ". /etc/profile && pyenv activate cv-env && cd /my-cv/src/build-engine && python -m build_engine -a complete-cv"
Documents build started
        Localizing complete-cv for en-US
        Building complete-cv for en-US
                Executing: $(which lualatex) -interaction=nonstopmode cv.tex for complete-cv
                Executing: $(which biber) cv.tex for complete-cv
                Executing: $(which lualatex) -interaction=nonstopmode cv.tex for complete-cv
                Executing: mv cv.pdf ${DOC_NAME}-${DOC_LOCALIZER}.pdf for complete-cv
                Executing: $(which bash) ${SHRINK_PDF_PATH} ${DOC_NAME}-${DOC_LOCALIZER}.pdf ${DOC_NAME}-${DOC_LOCALIZER}-compressed.pdf 120 for complete-cv
                Executing: mv ${DOC_NAME}-${DOC_LOCALIZER}.pdf ${RELEASE_DIR_PATH} for complete-cv
                Executing: mv ${DOC_NAME}-${DOC_LOCALIZER}-compressed.pdf ${RELEASE_DIR_PATH} for complete-cv
        ...
Documents build finished
Removing Google Drive uploaded pdfs
        Folders and files in Google Drive:
        Removing: {'mimeType': 'application/pdf', 'id': '1As-KATMWULXIRwPJXcc9EvA1DFAoAt_I', 'name': 'resume-cv-devops-es-ES-compressed.pdf'}
        Successfully deleted file/folder with ID: 1As-KATMWULXIRwPJXcc9EvA1DFAoAt_I
        ...
Google Drive pdfs removed
Documents upload started
        Uploading /my-cv/src/build-engine/build_engine/../../../.build/release/complete-cv/complete-cv-en-US.pdf file
        File {'kind': 'drive#file', 'id': '1KkRK2vUz6rqyL7vz2nJ6YALvtmODNX62', 'name': 'complete-cv-en-US.pdf', 'mimeType': 'application/pdf'} uploaded
        File /my-cv/src/build-engine/build_engine/../../../.build/release/complete-cv/complete-cv-en-US.pdf uploaded
        ...
Documents upload finished
```

## CI/CD

* Artifacts are builded by using [circle-ci](https://circleci.com/) reusing the docker compose approach for every job inside that platform.
* The `.circleci/config.yml` contains every step done to build and deploy artifacts.
* Since circle-ci's docker does not allow mount volumes. The `CIRCLE_CI` environment variable determines which `.yml` file to use for docker compose and the way that environment variables are obtained (from a file or direct O.S. / container consumption).
* Mounting volumes is necesary in the local container deployment to reflect file changes between host and container after saving them, no matter where those changes (host and or container) came from.
* Each time a new change is introduced and, depending of the circle-ci's configuration, a job is executed. For now, the CircleCI's job is executed when a commit is added / updated.

## Important notes

* For any reazon, if you need to update the docker compose `.yml` file, make sure to reflect the changes in both: `docker-compose.yml` and `docker-compose-circle-ci.yml` except for volume mounting because this feature is not allowed (and not needed) by circle-ci.

## Improvement opportunities

* Improve python scripting to build directly the `latex` directory if `localization` json file is missing, or a json file is corrupted or can not be readed.
* Improve python scripting to build the documents that user wants through input parameters. (Partially done, is supporting only one asset, but seems to be enough)
* Write tests on python.
* Send generated artifacts to a server of common downloading. (Done: Now supports Google Drive)

## PDFs release location

You can find the build PDFs released [here](https://drive.google.com/drive/folders/141Xvkm3HESrzg0tqTvyzlSyTiallZ2ae) 

## Author

* **Jhon Jairo Alvarez Londoño** - *Owner* - [jjalvarezl](https://github.com/jjalvarezl)

## LaTeX Templates used

* The`complete-cv` template was taken from <https://github.com/akanosora/friggeri-cv>
* The `resume-cv` template was taken from <https://github.com/GiantMolecularCloud/my-resume>
