﻿Function run-services {
    docker-compose.exe -p "build-engine" up -d  --build
}

Function build-docs {
    docker-compose.exe -p "build-engine" exec -it builder /bin/bash '-l' -c `
		". /etc/profile && pyenv activate cv-env && cd ./src/build-engine && python -m build_engine"
}

Function builder-start-session {
    docker-compose.exe -p "build-engine" exec -it builder /bin/bash '-l'
}

Function clean {
    rm -R ../.build
}