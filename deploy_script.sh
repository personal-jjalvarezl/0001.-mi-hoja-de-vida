#!/usr/bin/expect -f
set timeout 999
set username [lindex $argv 0];
set host [lindex $argv 1];
set password [lindex $argv 2];
set destinationPath [lindex $argv 3];
set buildsPath [lindex $argv 4];
eval spawn sftp $username@$host
expect "Are you sure you want to continue connecting (yes/no)?"
send "yes\n"
expect "$username@$host's password:"
send "$password\n"
expect "sftp>"
send "cd $destinationPath\n"
expect "sftp>"
send "lcd $buildsPath/\n"
expect "sftp>"
send "mput *.pdf\n"
expect "sftp>"
send "bye\n"